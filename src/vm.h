#ifndef VM_H
#define VM_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>


#define VERBOSE	1		/* whether this should run in verbose mode */
#define MAGIC 0x7577757e	/* "magic number" aka format specifier (== "uwu~") */
#define MAGIC_SIZE 4 		/* the number of bytes in the magic number */
// TODO: research a good size for this
#define MAX_STACK 1000		/* maximum number of bytes in the stack */
#define MAX_CALL_STACK 100 	/* maximum number of frames in the call stack */
#define MAX_LOCALS 10		/* maximum number of local variables */

typedef enum {
	      OP_HL  = 0x00, 	/* halt */
	      OP_LI  = 0x01,	/* load int */
	      OP_ADI = 0x02,	/* add ints */
	      OP_CL  = 0x03, 	/* call function */
	      OP_RT  = 0x04, 	/* return */
	      OP_LGV = 0x05, 	/* load global variable */
	      OP_SGV = 0x06,	/* store global variable */
	      OP_LLV = 0x07, 	/* load local variable */
	      OP_JMP = 0x08,	/* jump to instr address */
	      OP_PUL = 0x09, 	/* push locals frame */
	      OP_POL = 0x0a,	/* pop locals frame */
} opcode;


typedef unsigned char byte;

typedef struct {
  int ret;
  int lp;
  byte locals[MAX_LOCALS];
} call;

typedef struct {
  byte* code;
  size_t code_len;

  byte* globals;
  size_t global_len; // *not* the number of globals -- just the number of bytes

  byte stack[MAX_STACK];
  call call_stack[MAX_CALL_STACK];

  int pc; // program counter
  int sp; // stack pointer
  int csp; // call stack pointer
  int gp; // global variable pointer

  bool running;
} vm;


void panic(char* msg);

vm* new_vm(byte* code, size_t code_size, size_t globals_size);
void delete_vm(vm* v);

byte fetch(vm* v);
byte fetch_next(vm* v);

void push(vm* v, byte b);
byte pop(vm* v);
byte peek(vm* v);

void execute(vm* v);
void run(vm* v);

void print_stack(vm* v);
void push_call(vm* v, int nlocs);
void pop_call(vm* v);
void jump(vm* v, int addr);

void set_local(vm* v, int addr, byte b);
byte get_local(vm* v, int addr);
void set_global(vm* v, byte b);
byte get_global(vm* v, int addr);

#endif
