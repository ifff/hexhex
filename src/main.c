#include <stdio.h>
#include <stdlib.h>
#include "vm.h"

int main(int argc, char** argv) {
  FILE* f;
  long fsize;
  byte* buffer;
  size_t result;
  vm* v;
  int magic;
  byte globals_size;
  
  if (argc != 2) {
    printf("(err) %s found %d arguments, but expected 1.\n", argv[0], argc-1);
    puts("... quitting.");
    return 0;
  }

  f = fopen(argv[1], "r");

#ifdef VERBOSE
  printf("opening file at %s...\n", argv[1]);
#endif
  
  if (f == NULL) {
    printf("(err) cannot find file `%s'\n", argv[1]);
    puts("... quitting.");
    return 0;
  }

  // get the file size
  fseek(f, 0, SEEK_END);
  fsize = ftell(f);
  rewind(f);

  // allocate the buffer
  buffer = malloc(sizeof(byte) * fsize);
  if (buffer == NULL) {
    fputs("memory error", stderr);
    exit(2);
  }

  // read the file contents as bytes into the buffer
  result = fread(buffer, 1, fsize, f);
  if (result != fsize) {
    fputs("reading error", stderr);
    exit(3);
  }

#ifdef VERBOSE
  puts("read file contents successfully");
#endif

  // check the magic number
  magic = (buffer[3] << 24) | (buffer[2] << 16) | (buffer[1] << 8) | buffer[0];
  if (magic != MAGIC) {
    printf("(err) wrong exec format: expected 0x%x, but got 0x%x\n", MAGIC, magic);
    puts("... quitting\n");
    return 0;
  }

  // get the size of the global variables section (comes after magic number)
  globals_size = buffer[4];

#ifdef VERBOSE
  puts("found valid magic number");
#endif

  // create the vm
  v = new_vm(buffer, fsize, globals_size);
#ifdef VERBOSE
    puts("initialized virtual machine.");
#endif

#ifdef VERBOSE
  puts("running virtual machine...");
  puts("==========================");
#endif

  // run it
  run(v);

#ifdef VERBOSE
  puts("==========================");
  puts("cleaning up...");
#endif

  fclose(f);
  free(buffer);
  delete_vm(v);

#ifdef VERBOSE
  puts("program finished successfully.");
#endif

  return 0;
}
