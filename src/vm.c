#include "vm.h"

/**
 * creates a new virtual machine.
 * 
 * `new_vm` allocates space for a new virtual machine, and loads the
 * provided instructions into it.
 * 
 * @param instrs is an array of instructions.
 * @param num_instrs is the number of instructions.
 * @return a pointer to a new virtual machine with instructions loaded.
 */
vm* new_vm(byte* code, size_t code_size, size_t globals_size) {
  // the size of the entire "data" section preceding the instructions
  // (the magic number, and a single byte describing how many global variables exist)
  int offset = MAGIC_SIZE + 1;

  // allocate the vm and initialize the registers
  vm* v = (vm*)malloc(sizeof(vm));
  v->pc  = -1;
  v->sp  = -1;
  v->csp = -1;
  v->gp = -1;
  v->running = true;

  // allocate the globals
  v->global_len = globals_size;
  v->globals = (byte*)malloc(sizeof(byte) * v->global_len);
  
  // allocate the instructions
  v->code_len = code_size - offset;
  v->code = (byte*)malloc(sizeof(byte) * v->code_len);
  // load the instructions
  for (int i = 0; i < v->code_len; i++) {
    byte b = code[i + offset];
#ifdef VERBOSE
    printf("loaded %02x\n", b);
#endif
    v->code[i] = b;
  }

  return v;
}

/**
 * destroys a virtual machine and frees up the memory it occupied.
 * 
 * @param v is a pointer to the virtual machine that will be destroyed.
 */
void delete_vm(vm* v) {
  free(v->code);
  free(v);
}

/**
 * retrieves the byte at the current program counter.
 * 
 * @param v is the virtual machine that the instruction will be retrieved from.
 * @return the byte indexed by the program counter.
 */
byte fetch(vm* v) {
  if (v->pc < 0 || v->pc > v->code_len)
    panic("attempted to fetch data outside of instructions.");
  
  return v->code[v->pc];
}

/**
 * increments the program counter, and then fetches the byte at its position.
 * 
 * @param v is the virtual machine that the instruction will be retrieved from.
 * @return the byte indexed by the program counter.
 */
byte fetch_next(vm* v) {
  v->pc++;
  return fetch(v);
}

void push(vm* v, byte b) {
  if (++v->sp > MAX_STACK)
    panic("stack overflow.");
  
  v->stack[v->sp] = b;
}


byte pop(vm* v) {
  if (v->sp < 0)
    panic("stack underflow.");
  
  return v->stack[v->sp--];
}

byte peek(vm* v) {
  return v->stack[v->sp];
}

/**
 * executes the virtual machine's current instruction.
 *
 * @param v is the virtual machine.
*/
void execute(vm* v) {
  byte op = fetch_next(v);

  // REMEMBER: the operands come off the stack in reverse order
  switch (op) {
    // halt the program
  case OP_HL:
    v->running = false;
#ifdef VERBOSE
    printf("hl");
#endif
    break;

    // push an integer onto the stack
  case OP_LI: {
    int a = fetch_next(v);
    push(v, a);
#ifdef VERBOSE
    printf("li %d", a);
#endif
    break;
  }

    // pop two integers off the stack and push their sum
  case OP_ADI: {
    int b = pop(v);
    int a = pop(v);
    push(v, a + b);
#ifdef VERBOSE
    printf("adi %d", op);
#endif
    break;
  }

    // push a new call to the stack and jump to its address
  case OP_CL: {
    int addr  = fetch_next(v);
    int nargs = fetch_next(v);
    int nlocs = fetch_next(v);
    // make a new call thing
    push_call(v, nargs+nlocs);
    // copy the arguments to the start of locals
    for (int i = 0; i < nargs; i++)
      set_local(v, i, pop(v));
    // jump to the function address
    jump(v, addr);
    break;
#ifdef VERBOSE
    printf("cl %d %d %d", addr, nargs, nlocs);
#endif
  }

    // pop a call off the stack and jump to its return address
  case OP_RT: {
    jump(v, v->call_stack[v->csp].ret);
    pop_call(v);
#ifdef VERBOSE
    printf("rt %d", v->pc);
#endif
    break;
  }

    // load a global variable and push it onto the stack
  case OP_LGV: {
    int i = fetch_next(v);
    int a = get_global(v, i);
    push(v, a);
#ifdef VERBOSE
    printf("lgv %d", i);
#endif
    break;
  }

    // pop something off the stack and store it as a global variable
  case OP_SGV: {
    int a = pop(v);
    set_global(v, a);
#ifdef VERBOSE
    printf("sgv");
#endif
    break;
  }

    
    // load a local variable and push it onto the stack
  case OP_LLV: {
    int i = fetch_next(v);
    int a = get_local(v, i);
    push(v, a);
#ifdef VERBOSE
    printf("llv %d", i);
#endif
    break;
  }

    // jump to the given address
  case OP_JMP: {
    int a = fetch_next(v);
    jump(v, a);
#ifdef VERBOSE
    printf("jmp %d", a);
#endif
    break;
  }

    // push frame of local variables
    // (copies previous frame's contents, if there are any)
  case OP_PUL: {
    int nlocs = fetch_next(v);
    push_call(v, nlocs);
    // copy the stack to the start of locals
    for (int i = 0; i < nlocs; i++)
      set_local(v, i, pop(v));
#ifdef VERBOSE
    printf("pul %d", nlocs);
#endif
    break;
  }
    
    // pop current frame of local variables
  case OP_POL: {
    if (v->csp < 0)
      panic("cannot pop local frame because none exists");
    
    v->csp--;
#ifdef VERBOSE
    printf("pol");
#endif
    break;
  }

  default: {
    char err[50];
    sprintf(err, "received unknown instruction: %02x\n", op);
    panic(err);
    break;
  }

  }

#ifdef VERBOSE
  printf("\t");
  print_stack(v);
  printf("\n");
#endif
}


/**
 * adds a new call to the vm's call stack.
 * 
 * @param v is the virtual machine whose call stack will be manipulated.
 * @param nlocals is the number of local variables that will be used.
 */
void push_call(vm* v, int nlocs) {
  // check to ensure that there aren't too many local vars
  if (nlocs > MAX_CALL_STACK)
    panic("attempted to allocate too many local variables.");

  if (++v->csp > MAX_CALL_STACK)
    panic("attempted to allocate too many call frames");
  
  v->call_stack[v->csp].ret = v->pc;
}


/**
 * pops the current call from vm's call stack.
 * 
 * @param v is the virtual machine whose call stack will be manipulated.
 */
void pop_call(vm* v) {
  if (--v->csp < 0)
    panic("attempted to pop call frame without a current call frame in place");
}


/**
 * runs the program in the provided virtual machine, until it halts.
 *
 * @param v is the virtual machine that will be run.
 */
void run(vm* v) {
  while (v->running)
    execute(v);
}

/**
 * prints out the stack contents of the vm.
 *
 * @param v is the virtual machine whose stack contents will be printed.
 */
void print_stack(vm* v) {
  printf("[ ");
  for (int i = 0; i <= v->sp; i++)
    printf("%d ", v->stack[i]);
  printf("]");
}


/**
 * jumps the vm's program counter to the provided address.
 * if the address is out of bounds, the program will panic.
 * 
 * @param v is the virtual machine whose program counter will be modified.
 * @param addr is the address that the program counter will be set to.
 */
void jump(vm* v, int addr) {
  if (addr < 0 || addr > v->code_len)
    panic("attempted to jump past instruction set.");

  v->pc = addr-1;
}


/**
 * sets the value of a local variable in the current call stack.
 * 
 * @param v is the virtual machine that will be manipulated.
 * @param addr is the address of the local variable.
 * @param b is the value that the local variable will be set to.
 */
void set_local(vm* v, int addr, byte b) {
  if (addr < 0 || addr > MAX_LOCALS)
    panic("attempted to use out-of-bounds local variable address.");

  v->call_stack[v->csp].locals[addr] = b;
}


/**
 * retrieves the value of a local variable in the current call stack.
 * 
 * @param v is the virtual machine that will be manipulated.
 * @param addr is the address of the local variable.
 */
byte get_local(vm* v, int addr) {
  if (addr < 0 || addr > MAX_LOCALS)
    panic("attempted to use out-of-bounds local variable address.");

  return v->call_stack[v->csp].locals[addr];
}


/**
 * sets the value of a global variable.
 * 
 * @param v is the virtual machine that will be manipulated.
 * @param b is the value that the global variable will be set to.
 */
void set_global(vm* v, byte b) {
  if (++v->gp < 0 || v->gp > v->global_len)
    panic("attempted to use out-of-bounds global variable address.");

  v->globals[v->gp] = b;
}


/**
 * retrieves the value of a global variable.
 * 
 * @param v is the virtual machine that will be manipulated.
 * @param addr is the address of the global variable.
 */
byte get_global(vm* v, int addr) {
  if (addr < 0 || addr > v->global_len)
    panic("attempted to use out-of-bounds global variable address.");

  return v->globals[addr];
}


/**
 * prints an error message and then exits the program with a failure status.
 * 
 * @param msg is the message that is printed before the program exits.
 */
void panic(char* msg) {
  printf("error: %s\n", msg);
  puts("exiting program.");
  exit(EXIT_FAILURE);
}
