with (import <nixpkgs> {});
stdenv.mkDerivation rec {
   name = "hexhex";
   buildInputs = [
     gcc
     gnumake
   ];
}
