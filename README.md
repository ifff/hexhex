# hexhex
A stack-based virtual machine. The executable is located at `bin/hxx`.

# oh no!
This project was originally part of a programming language I'm working on called "hex". However, I decided to make the hex compiler generate
LLVM IR instead of hexhex bytecode, so hexhex is just sort of sitting here for the time being. As of right now, it can do some basic integer
operations, jumps, and calls.

## hexhexhex
Currently, `hexhexhex` isn't a thing.


<!-- ## design goals -->
<!-- ### simplicity -->
<!-- Unlike bigger sets of bytecode instructions (think JVM), `hexhex` should be easily described by a standard using simple language -- it shouldn't look like a legal document. Check out the wiki for the spec. Ultimately, the -->
<!-- simplicity of `hexhex` should allow it to be easily re-implemented by programmers without too much experience (kind of like Lua, but even simpler). -->
<!-- ### cuteness -->
<!-- Have you ever gotten angry because programmers don't have a sense of style? If so, prepare to be ~wowed~ by the aesthetic of `hexhex`. How will it achieve cuteness? -->
<!-- * documentation written with simple, unpretentious language -->
<!-- * short instruction names, preferably pronounceable using only one nice-sounding syllable and up to three chars in maximum length -->
<!-- * sigils, but only the cute ones -- not $, !, etc -->

<!-- ## planned features -->
<!-- ### interpreter -->
<!-- The interpreter will read and execute bytecode. -->
<!-- ### disassembler -->
<!-- This will print out bytecode in a more human-readable format. Documentation for this format can be found on the wiki. -->
